Newsletter Export is a Drupal module which allows one or more specific node
type to be exported as raw HTML. This is excellent for building a node type
to use as a newsletter. We generally use the References or Entity References
module to collect other content/articles into the export. The general
approach and how to set up your content type is explained here:
http://www.freeform.ca/en/blog/updated-recipe-building-newsletters-drupal-7

Visit admin/config/media/newsletter_export to choose the content types.
A new display mode "newsletter" is then available on those content types
so you can create a unique field arrangement and markup just for the
newsletter export.

Templates:
-------------------------------------------------------------------------------
The template file or files allow you to customize the output of your fields.
Move or copy newsletter-export.tpl.php to your theme folder to edit.

1) Template File (By default applies to all active newsletter content types): 
newsletter-export.tpl.php

2) Copy Template File (for a particular content type): 
newsletter-export--my_node_type.tpl.php
EG. newsletter-export--newsletter.tpl.php
(see "Using with Display Suite's layouts" further down for more information on using Display Suite templates)

The template provides three variables

$raw_markup -> The raw markup that drupal would provide for your node,
               outputs everything with one easy print statement.
$fields     -> An array of all the fields and their values
$node       -> The entire node object

The default template uses $raw_markup. One example of how to use the individual
fields wrapped with html and inlined css is shown below and with more detail on:
https://drupal.org/node/2045961

<html>
<head></head>
<body>
<h1 style="font-size: 16px; font-weight: bold; color: navy; margin-bottom: 10px;"><?php print $fields['title']; ?></h1>

<div style="font-size 10px;"><?php print $fields['body']['und'][0]['safe_value']; ?></div>
<hr/>
<?php foreach ($fields['field_news_latest_pages']['und'] as $delta => $news_item): ?>
    <?php $ent = $news_item['entity']; ?>
    <div style="font-size 10px; border-bottom: 1px dotted #808080;">
        <?php print $ent->body['und'][0]['safe_value']; ?>
    </div>
<?php endforeach; ?>
</body>
</html>

Note though that currently, relative URLs in content are only changed to absolute URLs in
$raw_markup. So if you want to use $fields or $node, you may have to add some code to 
your template to address this issue.

Using with Display Suite's layouts:
-------------------------------------------------------------------------------

The Display Suite module (https://www.drupal.org/project/ds) comes with multiple
layouts for more advanced theming. As for other templates, you will want to copy
the desired template to your theme and customize as needed.

Let's, for example, use Display Suite's 2-column stacked layout. You will see in your
export that the classes are in place (for example 'group-right' and 'group-left'), what 
you'll want to do is replace these classes with their inline equivalent.

1) copy 'sites/all/modules/contrib/ds/layouts/ds_2col_stacked/ds-2col-stacked.tpl.php' 
to 'sites/all/themes/YOUR_THEME/templates'

2) Re-name the file 'ds-2col-stacked--node-newsletter-export.tpl.php' (so that it is 
used only by the export display)

3) anywhere you see a class you'll want to replace it with the css inline style equivalent,
for example:

Change: '<<?php print $left_wrapper ?> class="group-left<?php print $left_classes; ?>">'

To: '<<?php print $left_wrapper ?> style="float: left; width: 50%;">'

Tip: to find out the css used, you can either open 'sites/all/modules/contrib/ds/layouts/ds_2col_stacked/ds-2col-stacked.css'
or, if using the inline settings of the Newsletter Export module, while viewing your 
newsletter on the web, right-click on an element and select 'inspect element' to view 
the active css in Chrome or Firefox (you may need to enable developer tools).  

Automatically inlining the css:
-------------------------------------------------------------------------------
Email clients are notorious for ignoring css. This includes major clients like
Gmail. If you want to use $raw_markup to ouput your content rather than make a 
custom template it will output woth the ids and classes from your sites template
but no inline css. You can then use a third-party service to turn your html file
with css to inline styles that will look good in most email clients. 
E.g. http://premailer.dialect.ca/, http://inlinestyler.torchboxapps.com/.
You'll have to upload the exported newsletter file to the site to have it converted.

Or call a script like https://github.com/tijsverkoyen/CssToInlineStyles to 
transform it before downloading.

Upload the script to the newsletter_export module.
In top of newsletter-export.tpl.php include the script. E.g.:

  module_load_include('php', 'newsletter_export', 'CssToInlineStyles/css_to_inline_styles');

Add your css as a string. E.g.:

  $css = 'h1 { background: green;}';

Run the html and css through the script.

  $current_html = new CSSToInlineStyles($raw_markup,$css);
  $processed_html = $current_html->convert();

Then replace:

  <?php print $raw_markup; ?>

with:

  <?php print $processed_html; ?>